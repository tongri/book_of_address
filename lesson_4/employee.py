from csv import DictReader
from datetime import date


class Employee:
    def __init__(self, first_name, last_name, email=None, technologies=None, main_skill=None, main_skill_grade=None):
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.technologies = technologies
        self.main_skill = main_skill
        self.main_skill_grade = main_skill_grade

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    @staticmethod
    def get_workdays():
        now = date.today()
        month_start = date(now.year, now.month, 1)
        weekend = [5, 6]
        diff = (now - month_start).days + 1
        day_count = len([x for x in range(diff) if x not in weekend])
        return day_count

    @property
    def as_str(self):
        return "{}: {} | {}".format(self.__class__.__name__,
                                    self.full_name,
                                    self.get_workdays())

    @classmethod
    def from_csv(cls, fp):
        """
        + Read data from file
        + Compose data from file with properties
        + Generate object
        """
        obj_list = []
        with open(fp, 'r') as fp:
            file_data = [row for row in DictReader(fp)]
        for row in file_data:
            first_name, last_name = row['Full Name'].split(' ', 1)
            email = row['Email']
            technologies = row['Technologies'].split('|')
            main_skill = row['Main Skill']
            main_skill_grade = row['Main Skill Grade']
            obj_list.append(cls(
                first_name=first_name,
                last_name=last_name,
                email=email,
                technologies=technologies,
                main_skill=main_skill,
                main_skill_grade=main_skill_grade
            ))
        return obj_list


list_ = Employee.from_csv('employees.csv')

