from datetime import datetime


class MyAwesomeException(Exception):
    def __init__(self, data):
        super().__init__()
        self.data = data

    def oleg_time(self):
        print(str(self))
        print(datetime.now())


class UnableToWorkException(Exception):
    pass
