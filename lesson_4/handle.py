from lesson_4.custom_exceptions import MyAwesomeException


def not_oleg(name):
    if name.lower() == 'oleg':
        raise MyAwesomeException("Olegs are not allowed here.")


def check_age(age):
    if age < 18:
        raise MyAwesomeException("You are too young.")


def main():
    # name = input("Enter your name:    ")
    try:
        # not_oleg(name)
        age = int(input("Enter your age:    "))
        check_age(age)

    except MyAwesomeException as err:
        if 'reason' in err.data.keys:
            pass
    except Exception as err:
        print(err)
    else:
        print("After two years you will be:")
        print(age + 2)
    finally:
        print("Bye!")


if __name__ == '__main__':
    main()
