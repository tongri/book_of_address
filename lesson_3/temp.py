from datetime import date, timedelta

text = "qweruoiuqwoiur oiqweruoiuqwoiur oiqweruoiuqwoiur " \
       "oiqweruoiuqwoiur oiqweruoiuqwoiur oiqweruoiuqwoiur " \
       "oiqweruoiuqwoiur oi"

today = date.today() + timedelta(days=2)


def some_func():
    pass


class SomeClass:
    valid = True

    def calculate(self):
        pass


DATABASE_URL = "etc"
DEBUG = False


class AnotherClass:
    pass


obj = AnotherClass()
