from lesson_3.models.employee import Employee
from lesson_4.custom_exceptions import UnableToWorkException


class Programmer(Employee):

    def __init__(self, tech_stack, *args):
        super().__init__(*args)
        self.tech_stack = tech_stack

    def __str__(self):
        return "{}: {}".format(self.__class__.__name__,
                               self.name)

    def work(self):
        emp_work = super().work()[:-1]
        return emp_work + " and coding."


class Recruiter(Employee):
    pass


class Candindate:
    def work(self):
        raise UnableToWorkException('I\'m not hired yet')


if __name__ == '__main__':
    cand = Candindate()
    cand.work()
