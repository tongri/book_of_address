class Employee:
    def __init__(self, name, email):
        self.name = name
        self.email = email
        try:
            self.validate_email()
        except FileNotFoundError:
            print('email.txt file was not found. '
                  'No validation provided')
        self.save_email()

    def save_email(self):
        with open('email.txt', 'a') as f:
            f.write(self.email.lower())
            f.write("\n")

    def validate_email(self):
        with open('email.txt', 'r') as f:
            emails = f.read()
        if self.email.lower() in emails:
            raise ValueError

    def work(self):
        return "I go to the office."


if __name__ == '__main__':
    emp = Employee('Ihor', 'i.vnukoff@gmail.com')
