from datetime import datetime, timedelta




class Programmer(Employee):
    def __str__(self):
        return "{}: {}".format(self.__class__.__name__,
                               self.name)

    def work(self):
        emp_work = super().work()[:-1]
        return emp_work + " and coding."


def smthng():
    print(datetime.now())
