class Phone:

    def __init__(self, phone_number, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.phone_number = phone_number

    def calling(self):
        print('I\'m calling to {}'.format(self.phone_number))


class MusicPlayer:
    def __init__(self, song_list, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.song_list = song_list

    def play_music(self):
        print('I can play this songs: {}'.format(', '.join(self.song_list)))


class SmartPhone(Phone, MusicPlayer):
    def __init__(self, phone_number, song_list):
        super().__init__(phone_number=phone_number, song_list=song_list)
