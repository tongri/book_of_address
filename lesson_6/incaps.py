class Addressbook:
    def __init__(self, fp):
        self.fp = fp

    def _read_file(self):
        with open(self.fp, 'r') as fp:
            data = fp.read()
            return data

    def get_file_data(self):
        return self._read_file()

    def find_record(self, term):
        data = self._read_file()
        # do something


if __name__ == '__main__':
    ab = Addressbook('__init__.py')
    term = 'tt'
    ab.find_record(term)

    data = ab._read_file()
    for row in data.split('\n'):
        if term in row:
            print(row)
