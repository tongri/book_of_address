class UnappropriateTypeException(Exception):

    @staticmethod
    def __str__():
        return "No appropriate type"

class NumberExistsException(Exception):

    @staticmethod
    def __str__():
        return "The number already exists"

class AmountofParamsException(Exception):

    @staticmethod
    def __str__():
        return "Wrong number of parameters"

class PersonExistsException(Exception):

    @staticmethod
    def __str__():
        return "person already extists"

class OrganizationExistsException(Exception):

    @staticmethod
    def __str__():
        return "Organization already exists"

class SearchTermException(Exception):

    @staticmethod
    def __str__():
        return "Search term must have at least 5 symbols"

class Record:

    def __init__(self, phone_number, address=None):
        self.phone_number = phone_number  # this should be unique
        self.address = address

    def __str__(self):
        raise NotImplementedError

    @classmethod
    def from_csv(cls, fp):
        raise NotImplementedError


class Person(Record):
    def __init__(self, phone_number, address, first_name, last_name, email):
        super().__init__(phone_number, address)
        self.first_name = first_name
        self.last_name = last_name
        self.email = email  # this should be unique

    @classmethod
    def from_csv(cls, fp):
        with open(fp, "r") as file:
            people = []
            for line in file:
                if line[0].lower() == "p":
                    person_data = [x.strip() for x in list(line.rstrip().split(","))[1:] if x]

                    AddressBook.validate_data(person_data, "person")
                    if len(person_data) == 4:
                        person_data.insert(1, None)
                    person = cls(*person_data)
                    people.append(person)

        return people


class Organization(Record):
    def __init__(self, phone_number, address, name, category):
        super().__init__(phone_number, address)
        self.name = name  # this should be unique
        self.category = category

    @classmethod
    def from_csv(cls, fp):
        with open(fp, "r") as file:
            orgs = []
            for line in file:
                if line[0].lower() == "o":
                    org_data = [x.strip() for x in list(line.rstrip().split(","))[1:] if x]

                    AddressBook.validate_data(org_data, "org")
                    if len(org_data) == 3:
                        org_data.insert(1, None)
                        print(org_data)
                    org = cls(*org_data)
                    orgs.append(org)

        return orgs


class AddressBook:
    def __init__(self, fp):
        self.fp = fp

    def validate_phone(self, phone):
        with open(self.fp, "r") as file:
            for line in file:
                if phone == list(line.split(","))[1].strip():
                    print(f'number {phone} already exists')
                    raise NumberExistsException

            else:
                return


    def validate_person(self, email):
        with open(self.fp, "r") as file:
            for line in file:
                if email == list(line.split(","))[-1].strip():
                    raise PersonExistsException

            else:
                return


    def validate_org(self, org_name):
        with open(self.fp, "r") as file:
            for line in file:
                if org_name == list(line.split(","))[3].strip():
                    raise OrganizationExistsException

            else:
                return

    @staticmethod
    def validate_data(data, type_):
        data = [x for x in data if x]
        if type_ == "org" and (len(data) == 3 or len(data) == 4):
            pass

        elif type_ == "person" and (len(data) == 4 or len(data) == 5):
            pass

        else:
            raise AmountofParamsException

    @staticmethod
    def validate_type(type_):
        if type_ != "org" and type_ != "person" and type_ != "all":
             raise UnappropriateTypeException

    @staticmethod
    def validate_search_term(search_term):
        if len(search_term) < 5:
            raise SearchTermException


    def add_record(self, type_, data):
        AddressBook.validate_data(data, type_)
        phone = data[0]
        AddressBook.validate_phone(self, phone)
        with open(self.fp, "a") as file:
            if type_ == "org":
                if len(data) == 4:
                    org_name = data[2]
                    AddressBook.validate_org(self, org_name)
                    file.write(f'{type_},{",".join(data)},,,\n')
                else:
                    org_name = data[1]
                    AddressBook.validate_org(self, org_name)
                    file.write(f'{type_},{data[0]},{",".join(data[1:])}\n')

            elif len(data) == 5:

                post_mail = data[-1]
                AddressBook.validate_person(self, post_mail)
                file.write(f'{type_}, {", ".join(data[:2])},,,{", ".join(data[2:])}\n')

            else:
                post_mail = data[-1]
                AddressBook.validate_person(self, post_mail)
                file.write(f'{type_}, {data[0]},,,,{", ".join(data[1:])}\n')
                

    def find_record(self, type_, search_term):

        AddressBook.validate_type(type_)
        AddressBook.validate_search_term(search_term)

        with open(self.fp, "r") as file:
            for line in file:
                print(f'{line[0]} and type is {type_}')
                full_str = [y for y in line.rstrip().split(",") if y]
                if search_term in line and type_ == "org" and line[0] == "o":
                    print(", ".join(full_str))
                    print("-"*10)

                elif type_ == "person" and line[0] == "p" and search_term in line:
                    print(", ".join(full_str))
                    print("-"*10)

                elif search_term in line and type_ == "all" and line[0] != "t":
                    print(", ".join(full_str))
                    print("-"*10)


    def get_records(self, type_):

        validate_type(type_)

        with open(self.fp, "r") as file:
            for line in file:
                full_str = [y for y in line.rstrip().split(",") if y]
                if type_ == "person" and line[0] == "p":
                    print(", ".join(full_str))
                    print("-"*10)

                elif type_ == "org" and line[0] == "o":
                    print(", ".join(full_str))
                    print("-"*10)

                elif line[0] != "t":
                    print(", ".join(full_str))
                    #print(", ".join(line.split(",")))
                    print("-"*10)


    def import_from_csv(self, fp):
        primary = open(self.fp, "a")
        new = open(fp, "r")

        for line_n in new:
            new_data = [x.strip() for x in list(line_n.rstrip().split(","))[1:]]
            try:
                phone_number = new_data[0]
                AddressBook.validate_phone(self, phone_number)
            except Exception:
                continue

            if line_n[0].lower() == "p":
                try:
                    type_ = "person"
                    AddressBook.validate_data(new_data, "person")

                except Exception:
                    continue
                else:
                    persons_email = new_data[-1]
                    AddressBook.validate_person(self, persons_email)

            elif line_n[0].lower() == "o":
                try:
                    type_ = "org"
                    AddressBook.validate_data(new_data, "org")
                except Exception:
                    continue
                else:
                    org_name = new_data[2]
                    AddressBook.validate_org(self, org_name)

            else:
                raise UnappropriateTypeException

            print("Adding the string:")
            print(f'{type_},{",".join(new_data)}\n')
            primary.write(f'{type_},{",".join(new_data)}\n')



def run():
    print("doin")
    a = Record("+38")
    a.str()

if __name__ == "main":
    run()
