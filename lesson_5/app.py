import sys
import traceback
import datetime as dt
from classes import Record, AddressBook, UnappropriateTypeException, AmountofParamsException, NumberExistsException, Person, Organization


def main():
    # CTRL + /
    address_book = AddressBook('address_book.csv')
    print("Address book initialized. Source is: {}".format(address_book.fp))

    while True:
        command = input("What you want to do? ADD, SHOW, FIND, EXIT\n").upper()
        """
        Validate input to check that input in these four: ADD, SHOW, FIND, EXIT
        1. Exit: close the program
        2. Add: add record
        3. Show: display records
        4. Find: find records
        """
        if command == 'ADD':
            """
            Ask user what type he want to add (org, person)
            based on this, ask required fields and check field uniqueness if required.
            After adding record show success message in console
            """
            type_ = input("what type do u want to add? (org, person)\n").lower().strip()
            if type_ == "org":
                data = list(x.strip() for x in input("Enter its phone, adress(optional),\
                 name, category (separate every param with coma)\n")
                    .split(","))


            elif type_ == "person":
                data = list(x.strip() for x in input("Enter its phone, address(optional),\
                     first name, last name, email - separate every param with coma\n").split(","))

            else:
                print(f"Wrong type {type_}")
                raise UnappropriateTypeException

            address_book.add_record(type_, data)

        if command == 'SHOW':
            """
            Ask type of records to show: org, person, all
            print records
            """
            type_ = input("what type do u want to be shown? (org, person, all)\n").lower().strip()

            address_book.get_records(type_)
        if command == 'FIND':
            """
            Ask for type of records to find: org, person, all
            Ask for string to find any text, at least 5 symbols
            print results
            """
            type_ = input("what type do u want to be shown? (org, person, all)\n").lower().strip()
            term = input("enter at least 5 text to find all the matches\n").lower().strip()
            address_book.find_record(type_, term)
        if command == 'EXIT':
            raise KeyboardInterrupt


if __name__ == '__main__':
    try:
        check = AddressBook("address_book.csv")
        check.import_from_csv("check.csv")
        main()
    # Add try/except block to log every unhandled exception. (same as for lesson 4)
    except Exception as err:
        with open('logs.py', "a") as file:
            tb = traceback.format_exc()
            day = dt.datetime.now().strftime("%Y-%m-%d %H:%m")
            file.write(f"date: {day}\n{str(err)}\n{type(err).__name__}\n{tb}")


