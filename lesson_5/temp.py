import random
import time

# Decorator is coming
from datetime import datetime


def check_time(func):
    def wrapper():
        print('I\'m in decorator')
        start = datetime.now()
        func()
        end = datetime.now()
        print('Takes {} seconds'.format((end - start).seconds))

    return wrapper


def do_something():
    wait = random.randint(0, 3)
    time.sleep(wait)
    print('Done')


# Deco 2
def add_tax(func):
    def wrapper(*args, **kwargs):
        tax = 0.05
        salary = func(*args, **kwargs)
        salary -= tax * salary
        return salary

    return wrapper


@add_tax
def calculate_salary(days, rate_per_hour):
    return days * 8 * rate_per_hour


'342.0 912.0 2660.0'
'360 960 2800'


class User:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        return "{} {}".format(self.first_name, self.last_name)

    @classmethod
    def from_string(cls, string):
        try:
            first_name, last_name = string.split(' ', 1)
        except ValueError:
            first_name = string
            last_name = ''

        return cls(first_name, last_name)


if __name__ == '__main__':
    result = add_tax(calculate_salary)(1, 5)
    print(result)
    print(calculate_salary(3, 15),
          calculate_salary(20, 6),
          calculate_salary(10, 35))

    user1 = User('John', 'Doe')
    user2 = User('Ivan', 'Ivanov')

    users_list = ['wqe qwe', 'qweqpoiwepoqi', 'qweoiqpw qweoip']

    new_users = [User.from_string(str_) for str_ in users_list]
    print(new_users)
    # print(user1.full_name)
    # print(user2.full_name)

    a = datetime.strptime('2020-22-04', '%Y-%d-%m')
    str_ = '2020-22-04'
    year, month, day = str_.split('-')
    b = datetime(year=2020, day=22, month=4)
    print(type(a))
    print(a)
